package com.dmitriev.restaurant.configintegration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.ConnectionFactory;

@Configuration
public class RestaurantConfiguration {
    @Autowired
    private ConnectionFactory connectionFactory;
    @Autowired
    private MessageConverter messageConverter;

    @Bean
    public IntegrationFlow startFlow() {
        return IntegrationFlows.from(
                Jms.messageDrivenChannelAdapter(Jms.container(connectionFactory, "INT.RESTAURANT.IN"))
                        .jmsMessageConverter(messageConverter))
                .handle(m -> System.out.println(m))
                .get();
    }
}
